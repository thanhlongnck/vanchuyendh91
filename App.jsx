/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {Provider} from 'react-redux';
import Index from './src/containers';
import {appStore} from './src/stores/appStore';

const App = () => {
  return (
    <Provider store={appStore}>
      <Index />
    </Provider>
  );
};

export default App;
