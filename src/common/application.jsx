export const country_code =  [
  {
    "countryCode": "US",
    "isRTL": false,
    "languageCode": "en",
    "languageTag": "en-US",
    "currency": "USD",
  },
  {
    "countryCode": "JP",
    "isRTL": false,
    "languageCode": "ja",
    "languageTag": "ja-JP",
    "currency": "JPY",
  },
  {
    "countryCode": "VN",
    "isRTL": false,
    "languageCode": "vi",
    "languageTag": "vi-VN",
    "currency": "VND",
  }
];