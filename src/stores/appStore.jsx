import {createStore, applyMiddleware} from 'redux';
import {reducers} from '../reducers/reducers';
import thunk from 'redux-thunk';

// store.js
export const appStore = createStore(
  reducers,
  applyMiddleware(thunk)
);
