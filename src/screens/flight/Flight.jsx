import React, { useEffect } from 'react';
import {ScrollView, View, Text, Button} from 'react-native';
import {connect} from 'react-redux';
import { Card } from 'react-native-elements';
import { fetchFlights } from '../../actions/flight/flightAction';

const Flight = (props) => {
  const {dispatch, flights} = props;
  // console.log(lang);
  // useEffect(() => {
  //   dispatch(fetchFlights());
  // }, [dispatch]);
  const handleShowFlights = () => {
    dispatch(fetchFlights());
  }
  return (
    <ScrollView>
      <Text>San bay di: NARITA</Text>
      <Text>San bay den: TAN SON NHAT</Text>
      <Text>Ngay bay di: 2020-10-09</Text>
      <Text>Ngay bay ve: 2020-11-09</Text>
      <Text>Loai tien: VND</Text>
      <Button
        title="Check flight"
        onPress={() => handleShowFlights()}
      />
      {flights && Object.keys(flights).length > 0 ? (
        <Card containerStyle={{padding: 1}} >
        {
          flights.map((u, i) => {
            return (
              <View key={i}>
                <Text>Gia tien: {u.MinPrice}</Text>
                <Text>Loai: </Text>{u.Direct ? <Text>bay thang</Text> : <Text>bay noi tuyen</Text>}
                <Text>Ngay bay di: {u.OutboundLeg.DepartureDate}</Text>
                <Text>Ngay bay ve: {u.InboundLeg.DepartureDate}</Text>
              </View>
            );
          })
        }
        </Card>
      ) : null}
    </ScrollView>
  );
};

const mapStateToProps = (state) => ({
  flights: state.flightReducer.flights,
});

export default connect(mapStateToProps)(Flight);
