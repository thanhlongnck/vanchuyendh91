import React from 'react';
import {Button, View, Text} from 'react-native';
import I18n from '../../utils/i18n';
// import en from '../../utils/locales/en';
// import ja from '../../utils/locales/ja';
// import vi from '../../utils/locales/vi';

export default function Lang(props) {
  const {route} = props;
  const {lang, changeLang, setChangeLang} = route.params;
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>{I18n.t('select_lang')}</Text>
      <Button title={I18n.t('en_lang')} onPress={() => {
        I18n.locale = 'en';
        setChangeLang(!changeLang);
        console.log(changeLang);
      }} />
      <Button title={I18n.t('vi_lang')} onPress={() => {
        I18n.locale = 'vi';
        setChangeLang(!changeLang);
        console.log(changeLang);
      }} />
      <Button title={I18n.t('ja_lang')} onPress={() => {
        I18n.locale = 'ja';
        setChangeLang(!changeLang);
        console.log(changeLang);
      }} />
    </View>
  );
}
