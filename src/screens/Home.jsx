import React from 'react';
import {Button, View, Text, Image, StyleSheet} from 'react-native';
import {appStore} from '../stores/appStore';
import {setLang} from '../actions/setting/langAction';
import I18n from '../utils/i18n';
// import {flight} from '../../assets/images/flight.png';

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  logo: {
    width: '100%',
    height: '80%',
  },
});

export default function Home(props) {
  const {navigation} = props;
  return (
    <View style={{flex: 1, flexDirection: 'column'}}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'powderblue',
        }}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View style={{flex: 1}}>
            <Image
              style={styles.logo}
              source={require('../../assets/images/flight.jpg')}
            />
            <Button
              title={I18n.t("flight_tab")}
              onPress={() => {
                navigation.navigate('FlightTab', {
                  screen: 'Flight'
                });
              }}
            />
          </View>
          <View style={{flex: 1}}>
            <Image
              style={styles.logo}
              source={require('../../assets/images/luggage.jpg')}
            />
            <Button
              title={I18n.t("send_tab")}
              onPress={() => {
                navigation.navigate('SendTab', {
                  screen: 'Send'
                });
              }}
            />
          </View>
        </View>
      </View>
      <View style={{flex: 1, backgroundColor: 'skyblue'}}>
        {/* <Text style={{marginBottom: 100}}>
          現在のstore: {JSON.stringify(appStore.getState())}
        </Text> */}
        {/* <Button title="set lang" onPress={() => setLang('jp')} /> */}
        <Button
          title="view state reducer"
          onPress={() => console.log(JSON.stringify(appStore.getState()))}
        />
      </View>
      <View style={{flex: 1, backgroundColor: 'steelblue'}} />
    </View>
  );
}
