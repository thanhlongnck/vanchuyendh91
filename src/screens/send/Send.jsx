import React, {useEffect} from 'react';
import {Button, View, Text} from 'react-native';
import I18n from '../../utils/i18n';

export default function Send(props) {
  const {navigation, route} = props;
  const {itemId, otherParam} = route.params;

  React.useEffect(() => {
    if (route.params?.post) {
      // Post updated, do something with `route.params.post`
      // For example, send the post to the server
    }
  }, [route.params]);
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>This is items screen</Text>
      <Text>itemId: {JSON.stringify(itemId)}</Text>
      <Text>otherParam: {JSON.stringify(otherParam)}</Text>
      <Text style={{margin: 10}}>Post: {route.params?.post}</Text>
      <Button
        title={I18n.t("flight_tab")}
        onPress={() => {
          navigation.navigate('Flight');
        }}
      />
      <Button
        title="Go to Send screen again"
        onPress={() => {
          navigation.navigate('Send', {
            itemId: Math.floor(Math.random() * 100),
            otherParam: 'anything you want here',
          });
        }}
      />
      <Button
        title="Go to Send detail 1"
        onPress={() => {
          navigation.navigate('SendDetail', {
            name: 'Send 1',
          });
        }}
      />
      <Button
        title="Go to Send detail 2"
        onPress={() => {
          navigation.navigate('SendDetail', {
            name: 'Send 2',
          });
        }}
      />
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}
