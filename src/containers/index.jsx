// home.js
import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import axios from 'axios';
import Home from '../screens/Home';
import Flight from '../screens/flight/Flight';
import FlightDetail from '../screens/flight/FlightDetail';
import Send from '../screens/send/Send';
import SendDetail from '../screens/send/SendDetail';
import Lang from '../screens/setting/Lang';
import {setLang} from '../actions/setting/langAction';
import {langs} from '../common/application';
import I18n from '../utils/i18n';

const Tab = createBottomTabNavigator();
const FlightStack = createStackNavigator();
const SendStack = createStackNavigator();
const SettingStack = createStackNavigator();

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#f4511e',
  },
  // headerTintColor: 'white',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});

// const getLang = (key) => key;

const Index = (props) => {
  const {dispatch, name} = props;
  useEffect(() => {
    axios
    .get('https://jsonplaceholder.typicode.com/todos/1', dispatch)
    .then((response) => response.data)
    .then((data) => {
      try {
        dispatch(setLang(langs[data.userId]));
      } catch (err) {
        return;
      }
    })
  }, [dispatch]);

  const headerStyle = {
    headerStyle: styles.headerStyle,
    headerTintColor: '#fff',
    headerTitleStyle: styles.headerTitleStyle,
  };

  const [changeLang, setChangeLang] = useState(false);
  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="HomeTab">
        <Tab.Screen
          name="HomeTab"
          options={{
            title: I18n.t("home_tab"),
            ...headerStyle,
          }}>
          {(props) => <Home {...props} />}
        </Tab.Screen>
        <Tab.Screen
          name="FlightTab"
          options={{
            title: I18n.t("flight_tab"),
            ...headerStyle,
          }}>
          {(props) => (
            <FlightStack.Navigator>
              <FlightStack.Screen name="Flight" component={Flight} />
              <FlightStack.Screen
                name="FlightDetail"
                component={FlightDetail}
              />
            </FlightStack.Navigator>
          )}
        </Tab.Screen>
        <Tab.Screen
          name="SendTab"
          options={{
            title: I18n.t("send_tab"),
            ...headerStyle,
          }}>
          {(props) => (
            <SendStack.Navigator>
              <SendStack.Screen
                name="Send"
                component={Send}
                initialParams={{itemId: 42}}
              />
              <SendStack.Screen name="SendDetail" component={SendDetail} />
            </SendStack.Navigator>
          )}
        </Tab.Screen>
        <Tab.Screen
          name="SettingTab"
          options={{
            title: I18n.t("setting_tab"),
            ...headerStyle,
          }}>
          {(props) => (
            <SettingStack.Navigator>
              <SettingStack.Screen
                name="Lang"
                component={Lang}
                initialParams={{lang: I18n.locale, changeLang, setChangeLang}}
              />
            </SettingStack.Navigator>
          )}
        </Tab.Screen>
      </Tab.Navigator>
    </NavigationContainer>
  );
};

const mapStateToProps = (state) => ({
  name: state.langReducer.name,
});


export default connect(mapStateToProps)(Index);
