import axios from 'axios';
import {GET_FLIGHT} from '../../constant/flight/flightConstant'
import { rapidAPIKey } from '../../common/api';

const getFlights = (data) => ({
  type: GET_FLIGHT,
  flights: data,
});

// export const fetchFlights = (endAirport, startAirport, startDate, endDate) => {
  export const fetchFlights = () => {
  return async (dispatch) => {
    try {
      const flights = await axios({
        // url: `https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browseroutes/v1.0/US/USD/en-US/${startAirport}/${endAirport}/${startDate}/${endDate}`,
        url: 'https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browseroutes/v1.0/VN/VND/vi-VN/NRT/SGN/2020-10-09/2020-11-09',
        method: 'GET',
        headers: {"X-RapidAPI-Key": rapidAPIKey}
      })
      // console.log(JSON.stringify(flights.data));
      const {data} = flights;
      const schedule = data.Quotes;
      dispatch(getFlights(schedule));
    } catch (error) {
      console.log(error)
    }
  }
}