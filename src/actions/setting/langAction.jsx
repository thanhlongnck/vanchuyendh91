import axios from 'axios';
import {SET_NAME} from '../../constant/setting/langConstant';

export const INITIAL_STATE = {
  name: 'Nanasi',
};

// 引数nameをとり、{type: "ADD_NAME", name: name}を返すjsの関数。
export const setName = (name) => ({
  type: SET_NAME,
  name,
});