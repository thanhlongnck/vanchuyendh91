import {
  GET_FLIGHT,
} from '../../constant/flight/flightConstant';

const flightReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_FLIGHT:
      return {...state, flights: action.flights};
    default:
      return state;
  }
};

export default flightReducer;
