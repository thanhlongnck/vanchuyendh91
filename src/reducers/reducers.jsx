import {combineReducers} from 'redux';
import langReducer from './setting/langReducer';
import flightReducer from './flight/flightReducer';

export const reducers = combineReducers({
  langReducer,
  flightReducer,
});
