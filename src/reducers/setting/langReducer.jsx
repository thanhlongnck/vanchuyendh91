import {
  SET_NAME,
} from '../../constant/setting/langConstant';
import {INITIAL_STATE} from '../../actions/setting/langAction';

const langReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_NAME:
      return {...state, name: action.name};
    default:
      return state;
  }
};

export default langReducer;
