export default {
  home_tab: 'ホーム',
  flight_tab: 'フライトを予約',
  send_tab: '荷物を送る',
  setting_tab: '設定',
  select_lang: '言語を選択',
  vi_lang: 'ベトナム語',
  ja_lang: '日本語',
  en_lang: '英語',
};
