export default {
  home_tab: 'Trang chủ',
  flight_tab: 'Đặt vé',
  send_tab: 'Gửi đồ',
  setting_tab: 'Cài đặt',
  select_lang: 'Chọn ngôn ngữ',
  vi_lang: 'Tiếng Việt',
  ja_lang: 'Tiếng Nhật',
  en_lang: 'Tiếng Anh',
};
