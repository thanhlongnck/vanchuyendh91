export default {
  home_tab: 'Home',
  flight_tab: 'Flight booking',
  send_tab: 'Send luggage',
  setting_tab: 'Setting',
  select_lang: 'Select language',
  vi_lang: 'Vietnamese',
  ja_lang: 'Japanese',
  en_lang: 'English',
};
