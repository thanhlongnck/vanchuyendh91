import React from 'react';
import {connect} from 'react-redux';

const Common = (props) => {
  const {lang} = props;
}
const mapStateToProps = (state) => ({
  lang: state.langReducer.lang,
});


export default connect(mapStateToProps)(Common);