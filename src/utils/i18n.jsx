import I18n from 'i18n-js';
import * as RNLocalize from 'react-native-localize';

import en from './locales/en';
import ja from './locales/ja';
import vi from './locales/vi';

const locales = RNLocalize.getLocales();
console.log(locales);

if (Array.isArray(locales)) {
  I18n.locale = locales[0].languageTag;
}

I18n.fallbacks = true;
I18n.translations = {
ja,
en,
vi,
};

export default I18n;
